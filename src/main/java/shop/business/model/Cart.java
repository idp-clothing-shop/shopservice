package shop.business.model;

import lombok.*;

import java.util.HashSet;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Cart {
	private Long id;
	private Set<CartItem> items = new HashSet<>();
}
