package shop.business.model;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Address {
	private Long id;
	private String city;
	private String street;
	private Long streetNumber;
}
