package shop.business.model;

public enum UserRole {
	ADMIN, CUSTOMER
}
