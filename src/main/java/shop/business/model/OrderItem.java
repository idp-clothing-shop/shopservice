package shop.business.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OrderItem {

	public enum Size {
		XXS, XS, S, M, L, XL, XXL
	}

	private Long id;
	private Product product;
	private Size size;
	private Order order;
	private int quantity;
}
