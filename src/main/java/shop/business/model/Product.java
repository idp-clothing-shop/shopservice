package shop.business.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Product {

	public enum Gender {
		WOMAN, MAN
	}

	private Long id;
	private String name;
	private Float price;
	private String material;
	private String image;
	private Gender gender;
}
