package shop.business.model;

import lombok.*;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Order {

	public enum OrderStatus {
		PROCESSING, SHIPPED, DELIVERED
	}

	private Long id;
	private Address address;
	private Set<OrderItem> items = new HashSet<>();
	private OrderStatus status;
	public LocalDateTime placementTime;
}
