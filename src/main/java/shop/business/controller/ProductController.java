package shop.business.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import shop.business.model.CartItem;
import shop.business.model.Product;
import shop.business.service.CartService;
import shop.business.service.ProductService;

import java.util.List;
import java.util.Optional;
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping(path = "api/shop")
public class ProductController {

	private final ProductService productService;
	private final CartService cartService;

	@Autowired
	public ProductController(ProductService productService, CartService cartService) {
		this.productService = productService;
		this.cartService = cartService;
	}

	@Operation(summary = "Show all products")
	@GetMapping
	public List<Product> getProducts() {
		return productService.getProducts();
	}

	@Operation(summary = "Get product by id")
	@GetMapping(path = "{productId}")
	public Optional<Product> getProduct(@PathVariable("productId") Long productId) {
		return productService.getProduct(productId);
	}

	@Operation(summary = "Add product in cart (CUSTOMER only)")
	@SecurityRequirement(name = "Authorization")
	@PreAuthorize("hasAuthority('CUSTOMER')")
	@PostMapping(path = "{productId}")
	public void addToCart(
			@PathVariable("productId") Long productId,
			@RequestParam(required = true) CartItem.Size size,
			@RequestParam(required = false) Integer quantity) {
		cartService.addToCart(productId, size, quantity);
	}

	@Operation(summary = "Show products in cart (CUSTOMER only)")
	@SecurityRequirement(name = "Authorization")
	@PreAuthorize("hasAuthority('CUSTOMER')")
	@GetMapping(path = "/cart")
	public List<CartItem> getCart() {
		return cartService.getCart();
	}

	@Operation(summary = "Remove item from cart (CUSTOMER only)")
	@SecurityRequirement(name = "Authorization")
	@PreAuthorize("hasAuthority('CUSTOMER')")
	@DeleteMapping(path = "/cart")
	public void deleteFromCart(@RequestParam Long itemId) {
		cartService.removeItem(itemId);
	}

	@Operation(summary = "Add new product (ADMIN only)")
	@SecurityRequirement(name = "Authorization")
	@PreAuthorize("hasAuthority('ADMIN')")
	@PostMapping
	public void registerNewProduct(@RequestBody Product product) {
		productService.addNewProduct(product);
	}

	@Operation(summary = "Delete product (ADMIN only)")
	@SecurityRequirement(name = "Authorization")
	@PreAuthorize("hasAuthority('ADMIN')")
	@DeleteMapping(path = "{productId}")
	public void deleteProduct(@PathVariable("productId") Long productId) {
		productService.deleteProduct(productId);
	}

	@Operation(summary = "Update product (ADMIN only)")
	@SecurityRequirement(name = "Authorization")
	@PreAuthorize("hasAuthority('ADMIN')")
	@PutMapping(path = "{productId}")
	public void updateProduct(
			@PathVariable("productId") Long productId,
			@RequestParam(required = false) String name,
			@RequestParam(required = false) Float price,
			@RequestParam(required = false) String material,
			@RequestParam(required = false) String image,
			@RequestParam(required = false) Product.Gender gender) {
		productService.updateProduct(productId, name, price, material, image, gender);
	}
}
