package shop.business.config;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.lang.NonNull;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.filter.OncePerRequestFilter;
import shop.business.dto.UserDTO;
import shop.business.model.User;

import java.io.IOException;

@Component
@RequiredArgsConstructor
public class JwtAuthenticationFilter extends OncePerRequestFilter {

	private final RestTemplate restTemplate;
//	@Value("${auth_service}")
	private String authService = "http://auth-service:8082/api/auth/business";

	@Override
	protected void doFilterInternal(
			@NonNull HttpServletRequest request,
			@NonNull HttpServletResponse response,
			@NonNull FilterChain filterChain
	) throws ServletException, IOException {

		final String authHeader = request.getHeader("Authorization");

		if (authHeader == null || !authHeader.startsWith("Bearer ")) {
			filterChain.doFilter(request, response);
			return;
		}

		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", authHeader);
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<UserDTO> responseEntity = restTemplate.exchange(
				authService, HttpMethod.GET, entity, UserDTO.class);

		if (responseEntity.getStatusCode() != HttpStatus.OK
				|| responseEntity.getBody() == null) {
			filterChain.doFilter(request, response);
			return;
		}

		UserDTO userDTO = responseEntity.getBody();
		UserDetails userDetails = new User(
				userDTO.getId(),
				userDTO.getFirstName(),
				userDTO.getLastName(),
				userDTO.getEmail(),
				userDTO.getUserRole());

		UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(
				userDetails,
				null,
				userDetails.getAuthorities());
		authToken.setDetails(
				new WebAuthenticationDetailsSource().buildDetails(request));
		SecurityContextHolder.getContext().setAuthentication(authToken);

		filterChain.doFilter(request, response);
	}
}