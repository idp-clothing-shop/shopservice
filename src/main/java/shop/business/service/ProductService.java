package shop.business.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.web.client.HttpClientErrorException;
import shop.business.exceptions.ConflictException;
import shop.business.exceptions.NotFoundException;
import shop.business.io.ProductIO;
import shop.business.model.CartItem;
import shop.business.model.Product;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class ProductService {

	private final ProductIO productIO;

	@Autowired
	public ProductService(ProductIO productIO) {
		this.productIO = productIO;
	}

	public List<Product> getProducts() {
		return productIO.findAll();
	}

	public void addNewProduct(Product product) {
		try {
			productIO.save(product);
		} catch (HttpClientErrorException.Conflict e) {
			throw new ConflictException("a product with id " + product.getId() + " already exists");
		}
	}

	public void deleteProduct(Long productId) {
		try {
			productIO.deleteById(productId);
		} catch (HttpClientErrorException.NotFound e) {
			throw new NotFoundException("product with id " + productId + " does not exist");
		}
	}

	public void updateProduct(Long productId,
								String name,
								Float price,
								String material,
								String image,
								Product.Gender gender) {

		Optional<Product> opt;

		try {
			opt = productIO.findById(productId);
		} catch (HttpClientErrorException.NotFound e) {
			throw new NotFoundException("product with id " + productId + " does not exist");
		}

		Product product = opt.get();

		if (name != null &&
				name.length() > 0 &&
				!Objects.equals(product.getName(), name)) {
			product.setName(name);
		}

		if (price != null &&
				!Objects.equals(product.getPrice(), price)) {
			product.setPrice(price);
		}

		if (material != null &&
				material.length() > 0 &&
				!Objects.equals(product.getMaterial(), material)) {
			product.setMaterial(material);
		}

		if (image != null &&
				image.length() > 0 &&
				!Objects.equals(product.getImage(), image)) {
			product.setImage(image);
		}

		if (gender != null &&
				gender != product.getGender()) {
			product.setGender(gender);
		}

		productIO.update(product);
	}

	public Optional<Product> getProduct(Long productId) {
		Optional<Product> opt;
		try {
			opt = productIO.findById(productId);
		} catch (HttpClientErrorException.NotFound e) {
			throw new NotFoundException("product with id " + productId + " does not exist");
		}
		return opt;
	}
}
