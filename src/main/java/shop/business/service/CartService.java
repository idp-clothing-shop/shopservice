package shop.business.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.server.ResponseStatusException;
import shop.business.io.CartIO;
import shop.business.model.CartItem;
import shop.business.model.User;

import java.util.List;

@Service
public class CartService {

	private final CartIO cartIO;

	@Autowired
	public CartService(CartIO cartIO) {
		this.cartIO = cartIO;
	}

	public void addToCart(Long productId, CartItem.Size size, Integer quantity) {
		User user = (User) SecurityContextHolder
				.getContext()
				.getAuthentication()
				.getPrincipal();
		try {
			cartIO.save(user.getId(), productId, size, quantity);
		} catch (HttpClientErrorException.NotFound e) {
			throw new ResponseStatusException(
					HttpStatus.NOT_FOUND,
					"Product with id " + productId + " does not exist");
		}
	}

	public List<CartItem> getCart() {
		User user = (User) SecurityContextHolder
				.getContext()
				.getAuthentication()
				.getPrincipal();
		return cartIO.getCartByUserId(user.getId());
	}

	public void removeItem(Long itemId) {
		User user = (User) SecurityContextHolder
				.getContext()
				.getAuthentication()
				.getPrincipal();
		try {
			cartIO.deleteItemById(user.getId(), itemId);
		} catch (HttpClientErrorException.NotFound e) {
			throw new ResponseStatusException(
					HttpStatus.NOT_FOUND,
					"Item with id " + itemId + " does not exist");
		}
	}
}
