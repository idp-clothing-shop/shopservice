package shop.business.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import shop.business.model.UserRole;

@Data
@AllArgsConstructor
public class UserDTO {
	private Long id;
	private String firstName;
	private String lastName;
	private String email;
	private UserRole userRole;
}
