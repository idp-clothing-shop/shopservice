package shop.business.io;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import shop.business.model.Product;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Component
public class ProductIO {

//	@Value("${io_service}")
	private String ioService = "http://io-service:8081/api/io";
	private String productsURL = ioService + "/products";

	@Autowired
	private RestTemplate restTemplate;

	public List<Product> findAll() {
		ResponseEntity<Product[]> response = restTemplate.getForEntity(productsURL, Product[].class);
		return Arrays.asList(response.getBody());
	}

	public boolean existsById(Long productId) {
		return findById(productId).isPresent();
	}

	public void deleteById(Long productId) {
		restTemplate.delete(productsURL + "/" + productId);
	}

	public Optional<Product> findById(Long productId) {
		Product product = restTemplate.getForObject(productsURL + "/" + productId, Product.class);
		Optional<Product> opt = Optional.empty();

		if (product != null) {
			opt = Optional.of(product);
		}

		return opt;
	}

	public void save(Product product) {
		HttpEntity<Product> requestBody = new HttpEntity<>(product);
		ResponseEntity<Product> response = restTemplate.postForEntity(productsURL, requestBody, Product.class);
	}

	public void update(Product product) {
		HttpEntity<Product> requestBody = new HttpEntity<>(product);
		restTemplate.exchange(productsURL + "/" + product.getId(),
							HttpMethod.PUT,
							requestBody,
							Void.class,
							product.getId());
	}
}
