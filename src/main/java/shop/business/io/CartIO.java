package shop.business.io;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import shop.business.model.CartItem;
import shop.business.model.User;

import java.util.Arrays;
import java.util.List;

@Component
public class CartIO {

//	@Value("${io_service}")
	private String ioService = "http://io-service:8081/api/io";
	private String cartURL = ioService + "/cart";

	@Autowired
	private RestTemplate restTemplate;

	public List<CartItem> getCartByUserId(Long userId) {
		HttpEntity<Long> httpEntity = new HttpEntity<>(null);

		UriComponentsBuilder uriBuilder = UriComponentsBuilder
				.fromHttpUrl(cartURL)
				.queryParam("userId", userId);

		ResponseEntity<CartItem[]> response = restTemplate.exchange(
				uriBuilder.toUriString(),
				HttpMethod.GET,
				httpEntity,
				CartItem[].class);

		return Arrays.asList(response.getBody());
	}

	public void save(Long userId, Long productId, CartItem.Size size, Integer quantity) {
		UriComponentsBuilder uriBuilder = UriComponentsBuilder
				.fromHttpUrl(cartURL + "/" + productId)
				.queryParam("userId", userId)
				.queryParam("size", size)
				.queryParam("quantity", quantity);

		ResponseEntity<CartItem[]> response = restTemplate.exchange(
				uriBuilder.toUriString(),
				HttpMethod.POST,
				null,
				CartItem[].class);
	}

	public void deleteItemById(Long userId, Long itemId) {
		UriComponentsBuilder uriBuilder = UriComponentsBuilder
				.fromHttpUrl(cartURL)
				.queryParam("user", userId)
				.queryParam("item", itemId);

		ResponseEntity<Void> response = restTemplate.exchange(
				uriBuilder.toUriString(),
				HttpMethod.DELETE,
				null,
				Void.class);
	}
}
