FROM amazoncorretto:17-alpine-jdk
EXPOSE 8080
COPY target/business-0.0.1.jar business.jar
ENTRYPOINT ["java", "-jar", "business.jar"]
